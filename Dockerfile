FROM node:18-slim
LABEL author="Team GeoGirafe"

ENV npm_config_cache=/tmp/npm

WORKDIR /src
ENTRYPOINT umask 002 && npm install && npm run build
