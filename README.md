# GeoGirafe Builder

This is a small image based on the standard node image.  
It just adds custom configuration like `umask` and `npm_config_cache` to allow a correct build of the application.

## Build this docker image

```
docker build -t geogirafe/builder .
```

## Build GeoGirafe with this image

```
docker run -u $(id -u):$(id -g) -v $PWD:/src geogirafe/builder bash -c "cd /src && npm install && npm run build"
```
